
package ab;

public class arbol {
   na raiz;
   
   public arbol(){
      raiz=null;
   }
   
   //insertar un nodo al arbol
   public void insertarnodo (int x, String n){
       na nuevo=new na (x,n);
       if(raiz==null){
           raiz=nuevo;
       }else{
         na auxiliar=raiz;
         na padre;
         while(true){
             padre=auxiliar;
             if(x<auxiliar.dato){
                 auxiliar=auxiliar.hijoI;
                 if(auxiliar==null){
                    padre.hijoI=nuevo;
                    return;
                 }
             }else{
                 auxiliar=auxiliar.hijoD;
                 if(auxiliar==null){
                     padre.hijoD=nuevo;
                     return;
                 }
             }
         }          
       }
   }
   public boolean vacio(){
       return raiz==null;
   }
   
   public void inorden (na l){
       if(l!=null){
           inorden(l.hijoI);
           System.out.println(l.dato);
           inorden(l.hijoD);
       }
   }
   
   public void preorden (na l){
       if(l!=null){
           System.out.println(l.dato);
           preorden(l.hijoI);
           preorden(l.hijoD);
       }
   }
   
   public void posorden(na l){
       if(l!=null){
           posorden(l.hijoI);
           posorden(l.hijoD);
           System.out.println(l.dato);
       }
   }
}
